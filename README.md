## PROYECTO I - Forocoches

Estos proyectos se basan en pequeños desafíos para aprender a programar en Python.
Enlace al post: https://www.forocoches.com/foro/showthread.php?t=7931052

## Desafío

Web con Flask, que al ejecutarse revise la hora actual. Si son más de las 12h saque un texto como "Són mas de las 12", y si no lo son, otro texto "Aún no son las 12".

---

## Participantes:

· R.Daneel
· Escopeto
· Kener
· wibo99
· samozite
· Forums
· Javirrulos
· acbpowa
· rySeeR
· rav3ns
· keyven